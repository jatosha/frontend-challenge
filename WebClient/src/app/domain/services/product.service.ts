import { Injectable } from '@angular/core';
import { Product } from '../models/product';

export interface IProductService {
    getAllProducts(): Promise<Product[]>;
    getProductById(productId: string): Promise<Product>;
}

@Injectable({
    providedIn: 'root'
})
export class ProductService implements IProductService {
    public async getAllProducts(): Promise<Product[]> {
        return []; // TODO: implement
    }
    public async getProductById(productId: string): Promise<Product> {
        return null as any; // TODO: implement
    }
}
