﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Jatosha.Challenge.DataAccess.Repository;

namespace Jatosha.Challenge.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private IProductRepository _repository;

        public ProductsController(IProductRepository repository)
        {
            this._repository = repository;
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllProducts()
        {
            return this.Ok(await this._repository.LoadAll());
        }

        [HttpGet("{productId:guid}")]
        public async Task<IActionResult> GetProduct(Guid productId)
        {
            var products = await this._repository.LoadAll();
            var product = products.FirstOrDefault(d => d.Id == productId);

            if (product != null)
            {
                return this.Ok(product);
            }

            return this.NoContent();
        }
    }
}