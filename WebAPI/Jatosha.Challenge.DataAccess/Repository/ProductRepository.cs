using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jatosha.Challenge.Models;

namespace Jatosha.Challenge.DataAccess.Repository
{
    public class DildoRepository : IProductRepository
    {
        public Task<IEnumerable<Product>> LoadAll()
        {
            IEnumerable<Product> products = new List<Product> {
                    new Product(new Guid("52699e2a-c95a-4ca9-9a8c-1ffdb076ca57"), "The Widowmaker", ProductSize.Large),
                    new Product(new Guid("092c08dc-4530-47cd-b5eb-c4ff69bfae7b"), "Hand of Exhaust", ProductSize.ExtraLarge),
                    new Product(new Guid("afe842e9-1ad0-453a-b3b2-4a827feb9d8e"), "Doctor Screw", ProductSize.SuperExtraLarge),
                    new Product(new Guid("642edce0-e643-4eba-86ef-c083559cb59a"), "The Inner Limits", ProductSize.Large),
                    new Product(new Guid("147c46e4-746f-49b5-93ba-c5ee7728b566"), "The D-800", ProductSize.ExtraLarge),
                    new Product(new Guid("bb173d69-d449-4849-9178-1b3b78d51985"), "Excalibur", ProductSize.SuperExtraLarge)
            };
            
            return Task.FromResult(products);
        }
    }
}
