﻿using System;

namespace Jatosha.Challenge.Models
{
    public class Product
    {
        public Guid Id { get; private set; }
        public String Name { get; private set; }
        public ProductSize Size { get; private set; }

        public Product(Guid id, String name, ProductSize size)
        {
            this.Id = id;
            this.Name = name;
            this.Size = size;
        }
    }
}